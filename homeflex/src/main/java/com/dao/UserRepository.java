package com.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("from User where userEmail=:email and userPassword=:password")
	User userLogin(@Param("email") String email,@Param("password") String password);
	
	Optional<User> findByuserEmail(String email);
}
