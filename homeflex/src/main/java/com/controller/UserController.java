package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;

@RestController
@RequestMapping("/users") // Base path for all user-related endpoints
public class UserController {
	
	@Autowired
	private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers(){
		return userDao.getAllUsers();
	}
	
	@PostMapping("/register")
	public User registerUser(@RequestBody User user){
		// Encrypt the password before saving to the database
		String encryptedPassword = bCryptPasswordEncoder.encode(user.getUserPassword());
        user.setUserPassword(encryptedPassword);
		return userDao.registerUser(user);
	}
	
}
