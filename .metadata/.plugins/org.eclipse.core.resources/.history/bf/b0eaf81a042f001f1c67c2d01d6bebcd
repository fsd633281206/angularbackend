package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {
	
	@Autowired
	private UserRepository userRepo;
	
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}

	public User registerUser(User user) {
		return userRepo.save(user);
	}
	
	public User userLogin(String email,String password){
		User user=userRepo.findByuserEmail(email).orElse(null);
		
		if(user!=null && passwordMatches(password,user.getUserPassword())){
			return user;
		}else{
			return null;
		}
	}
	
	private boolean passwordMatches(String rawPassword,String encodedPassword){
		return new BCryptPasswordEncoder().matches(rawPassword,encodedPassword);
	}
	
	public User changePassword(String email,String newPassword){
		User user=userRepo.findByuserEmail(email).orElse(null);
		
		if(user!=null){
			String encryptedPassword=new BCryptPasswordEncoder().encode(newPassword);
			
			user.setUserPassword(encryptedPassword);
			return userRepo.save(user);
		}else{
			return null;
		}
	}
}
